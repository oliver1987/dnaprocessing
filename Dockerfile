
FROM python:latest

COPY packages /app/packages
COPY router /app/router
COPY app.py /app/app.py
COPY config.yaml /app/config.yaml
COPY entrypoint.sh /app/entrypoint.sh
COPY requirements.txt /app/requirements.txt
ENV CONFIG_PATH="/app/config.yaml"
CMD pip install --upgrade pip
CMD pip install -r /app/requirements.txt
