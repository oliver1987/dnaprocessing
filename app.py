from fastapi import FastAPI

from router import eval_router

app = FastAPI()

app.include_router(eval_router.router)


