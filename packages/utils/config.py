import yaml
import os


def readconfig():
    cfg_path = os.getenv('CONFIG_PATH')
    with open(cfg_path) as stream:
        cfg = yaml.safe_load(stream)

    return cfg